import {ICat} from "../types/cats"
import Cat from "../models/cat";

interface CatRepo {
    getCats(): Promise<Array<ICat>>
}
class CatRepoImpl implements CatRepo {
    private constructor() {}
    static of(): CatRepoImpl {
        return new CatRepoImpl()
    }
    async getCats(): Promise<Array<ICat>> {
        return Cat.find()
    }
}
export { CatRepoImpl }
